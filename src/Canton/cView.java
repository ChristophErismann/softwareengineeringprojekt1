package Canton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import Canton.Canton.Language;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class cView {
	private Stage stage;
	private cModel model;

	// Controls used for data processing
	TextField tfName = new TextField();
	TextField tfPopulation = new TextField();
	TextField tfFoundationDate = new TextField();
	ComboBox<Language> cmbLanguage = new ComboBox<>();

	Label lblDataID = new Label();
	Label lblDataName = new Label();
	Label lblDataPopulation = new Label();
	Label lblDataLanguage = new Label();
	Label lblDataFoundation = new Label();

	// Fields of update Pane
	TextField tfUpdateCantonName = new TextField();
	TextField tfUpdatePopulation = new TextField();
	TextField tfUpdateFoundationDate = new TextField();
	ComboBox<Language> cmbUpdateLanguage = new ComboBox<>();
	public IntegerProperty IdObjectToUpdate;

	// Table
	SplitPane spListCantons;
	TableView<Canton> tableCantonView = new TableView<Canton>();
	TableColumn<Canton, StringProperty> columnCantonName;
	TableColumn<Canton, IntegerProperty> columnPopulation;
	TableColumn<Canton, ObjectProperty> columnFoundationDate;
	TableColumn<Canton, Button> columnShow;

	// Buttons
	Button btnSave = new Button("Anlegen");
	Button btnClear = new Button("Clear");
	Button btnUpdateChanges = new Button("Speichern");
	Button btnDelete = new Button("Loeschen");

	
	Stage updateStage = new Stage();

	public cView(Stage stage, cModel model) throws FileNotFoundException {
		this.stage = stage;
		this.model = model;

		BorderPane root = new BorderPane();
		VBox vbox = new VBox();
		vbox.getChildren().addAll(createDataEntryPane(), createControlPane());
		root.setLeft(vbox);
		root.setCenter(creatTableListPane());
		stage.setMinHeight(600);
		stage.setMaxWidth(900);

		Image imagePanorama = new Image(new FileInputStream("src/Canton/swisspanorama.jpg"));
		ImageView imageView = new ImageView();
		imageView.setImage(imagePanorama);
		imageView.isResizable();
		Group imageGroup = new Group(imageView);
		root.setBottom(imageGroup);

		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("c.css").toExternalForm());
		stage.setTitle("Kanton Verwaltung");
		
		stage.getIcons().add(new Image(new FileInputStream("src/Canton/swisscross.png")));
		stage.setScene(scene);
		stage.show();
	}

	public Pane creatTableListPane() {
		VBox pane = new VBox();
		tableCantonView.setEditable(true);

		// adds the column Kanton in table
		TableColumn<Canton, String> nameCol = new TableColumn<Canton, String>("Kanton");
		nameCol.setCellValueFactory((p) -> {
			return p.getValue().getNameProperty();
		});

		// adds the column Population in table
		TableColumn<Canton, Integer> populationCol = new TableColumn<Canton, Integer>("Bevoelkerung");
		populationCol.setCellValueFactory((p) -> {
			return p.getValue().getPopulationProperty().asObject();
		});

		// adds the column Foundation Date in table
		TableColumn<Canton, String> foundationDateCol = new TableColumn<Canton, String>("Gruendungsdatum");
		foundationDateCol.setCellValueFactory((p) -> {
			return p.getValue().getFoundationDateProperty().asString();
		});

		// adds the column Id in table
		TableColumn<Canton, Integer> idCol = new TableColumn<Canton, Integer>("Id");
		idCol.setCellValueFactory((p) -> {
			return p.getValue().getID().asObject();
		});
		// adds the column Language in table
		TableColumn<Canton, String> languageCol = new TableColumn<Canton, String>("Sprache");
		languageCol.setCellValueFactory((p) -> {
			return p.getValue().getLanguageProperty().asString();
		});

		// adds the colum Language in table
		TableColumn actionCol = new TableColumn("Bearbeiten");
		actionCol.setCellValueFactory(new PropertyValueFactory<>(""));

		// this method is adapted from:
		// https://docs.oracle.com/javafx/2/ui_controls/table-view.htm
		Callback<TableColumn<Canton, String>, TableCell<Canton, String>> cellFactory = new Callback<TableColumn<Canton, String>, TableCell<Canton, String>>() {
			@Override
			public TableCell call(final TableColumn<Canton, String> param) {
				final TableCell<Canton, String> cell = new TableCell<Canton, String>() {

					final Button btn = new Button("Bearbeiten");

					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
							setText(null);
						} else {
							btn.setOnAction(event -> {
								Canton canton = getTableView().getItems().get(getIndex());
								System.out.println(canton.getID() + " " + canton.getFoundationDateProperty());
								updateDataDisplayPane(canton);
							});
							setGraphic(btn);
							setText(null);
						}
					}
				};
				return cell;
			}
		};
		actionCol.setCellFactory(cellFactory);

		tableCantonView.getColumns().addAll(idCol, nameCol, populationCol, languageCol, foundationDateCol, actionCol);
		tableCantonView.setItems(model.cantonsObservableList);

		columnCantonName = new TableColumn<Canton, StringProperty>();
		columnPopulation = new TableColumn<Canton, IntegerProperty>();
		columnFoundationDate = new TableColumn<Canton, ObjectProperty>();
		columnShow = new TableColumn<Canton, Button>();

		pane.getChildren().add(tableCantonView);

		return pane;
	}

	public void start() {
		stage.show();
	}

	private Pane createDataEntryPane() {
		GridPane pane = new GridPane();
		pane.setId("dataEntry");
		// Declare the individual controls in the GUI
		Label lblName = new Label("Name");
		Label lblPopulation = new Label("Bevoelkerung");
		tfPopulation.setPromptText("ganze Zahl");
		Label lblLanguage = new Label("Sprache");
		Label lblFoundation = new Label("Gruendungsdatum");
		tfFoundationDate.setPromptText("yyyy-MM-dd");

		// Fill combos
		cmbLanguage.getItems().addAll(Canton.Language.values());

		// Organize the layout, add in the controls (col, row)
		pane.add(lblName, 0, 0);
		pane.add(tfName, 1, 0);
		pane.add(lblLanguage, 0, 1);
		pane.add(cmbLanguage, 1, 1);
		pane.add(lblPopulation, 0, 2);
		pane.add(tfPopulation, 1, 2);
		pane.add(lblFoundation, 0, 3);
		pane.add(tfFoundationDate, 1, 3);

		return pane;
	}

	private Pane createControlPane() {
		GridPane pane = new GridPane();
		pane.setId("controlArea");
		pane.add(btnSave, 0, 0);
		pane.add(btnClear, 1, 0);

		return pane;
	}

	public Pane updateDataDisplayPane(Canton canton) {
		updateStage.setWidth(300);
		updateStage.setHeight(400);
		GridPane pane = new GridPane();
		pane.setId("updateDataDisplay");

		// Declare the individual controls in the GUI
		Label lblInfo = new Label("Ändere hier deinen Kanton:");
		this.IdObjectToUpdate = canton.getID();

		Label lblNewCantonName = new Label("Name: ");
		this.tfUpdateCantonName = new TextField();
		this.tfUpdateCantonName.setText(canton.getNameProperty().getValue());

		Label lblNewPopulation = new Label("Bevoelkerung: ");
		this.tfUpdatePopulation = new TextField();
		this.tfUpdatePopulation.setText(canton.getPopulationProperty().getValue().toString());

		Label lblNewLanguage = new Label("Sprache: ");
		this.cmbUpdateLanguage = new ComboBox<>();
		this.cmbUpdateLanguage.getItems().addAll(Canton.Language.values());
		this.cmbUpdateLanguage.setValue(canton.getLanguageProperty().getValue());
		
		Label lblNewFoundationDate = new Label("Gruendungsdatum: ");
		this.tfUpdateFoundationDate = new TextField();
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMAN);
//		LocalDate date = LocalDate.parse(tfFoundationDate.getText(), formatter);
		this.tfUpdateFoundationDate.setText(canton.getFoundationDateProperty().getValue().toString());
		
		
		pane.add(lblInfo, 0, 0);
		pane.add(lblNewCantonName, 0, 1);
		pane.add(tfUpdateCantonName, 1, 1);
		pane.add(lblNewPopulation, 0, 2);
		pane.add(tfUpdatePopulation, 1, 2);
		pane.add(lblNewLanguage, 0, 3);
		pane.add(cmbUpdateLanguage, 1, 3);
		pane.add(lblNewFoundationDate, 0, 4);
		pane.add(tfUpdateFoundationDate, 1, 4);
		pane.add(btnUpdateChanges, 0, 5);
		pane.add(btnDelete, 1, 5);

		Scene scene = new Scene(pane);
		updateStage.setScene(scene);
		updateStage.show();
		return pane;
	}
}
