package Canton;

import java.time.LocalDate;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;

public class Canton {

	public enum Language {
		DEUTSCH, FRANZÖSISSCH, ITALIENISCH, RÄTROMANISCH
	};

	private static int highestID = -1;

	public static int getHighestID() {
		return highestID;
	}

	private final IntegerProperty ID;
	private StringProperty nameOfCanton;
	private ObjectProperty<Language> language;
	private IntegerProperty population;
	private ObjectProperty<LocalDate> foundationDate;

	private ObjectProperty<Button> btnEdit;

	public Canton(String name, int population, Language language, LocalDate ld) {
		this.ID = new SimpleIntegerProperty(getNextID());
		this.nameOfCanton = new SimpleStringProperty(name);
		this.population = new SimpleIntegerProperty(population);
		this.language = new SimpleObjectProperty<Language>(language);
		this.foundationDate = new SimpleObjectProperty<>(ld);

	}
	
	public ObjectProperty<LocalDate> dateProperty() {
		return foundationDate;
	}
	
	// Class method to get next available ID
	private static int getNextID() {
		highestID = highestID + 1;
		return highestID;
	}

	/// ---- Getters and Setters ----////

	public StringProperty getNameProperty() {
		if (nameOfCanton == null)
			nameOfCanton = new SimpleStringProperty("no Name");
		return nameOfCanton;
	}

	public IntegerProperty getPopulationProperty() {
		if (population == null)
			population = new SimpleIntegerProperty(0);
		return population;
	}

	public ObjectProperty<Button> getButtonProperty() {
		return btnEdit;
	}

	public ObjectProperty<LocalDate> getFoundationDateProperty() {
		return foundationDate;
	}
	
    public final LocalDate getFoundationDate() {
        return foundationDate.get();
    }

	public void setFoundationDateProperty(LocalDate setFoundationDate) {
		this.foundationDate = new SimpleObjectProperty<>(setFoundationDate) ;
	}
	
	public void setFoundationDate(LocalDate foundationDate) {
		this.foundationDate.set(foundationDate);
	}

	public ObjectProperty<Language> getLanguageProperty() {
		return language;
	}

	public void setNameOfCanton(String nameOfCanton) {
		this.nameOfCanton.set(nameOfCanton);
	}

	public void setLanguage(Language language) {
		this.language.set(language);
	}

	public void setPopulation(int population) {
		this.population.set(population);
	}

	public IntegerProperty getID() {

		return ID;
	}

	public String toString() {
		return this.nameOfCanton + " " + this.population + " " + this.language+ " "+this.foundationDate;

	}

}
