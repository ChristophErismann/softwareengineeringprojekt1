package Canton;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.Optional;

import Canton.Canton.Language;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class cController {
	private cView view;
	private cModel model;

	public cController(cModel model, cView view) {
		this.model = model;
		this.view = view;

		view.btnSave.setOnAction(this::save);
		view.btnClear.setOnAction(this::clear);
		view.btnUpdateChanges.setOnAction(this::update);
		view.btnDelete.setOnAction(this::delete);

	}

	private void save(ActionEvent e) {

	
		
		try {
			String name = view.tfName.getText();
			int population = Integer.parseInt(view.tfPopulation.getText());
			Language language = view.cmbLanguage.getValue();
			
			for(Canton c : model.cantonsObservableList) {
				System.out.println(c.getNameProperty());
				if(c.getNameProperty().getValue().equals(name)) {
				 Alert alert = new Alert(AlertType.ERROR);
				 alert.setContentText("Kanton existiert bereits");
				 alert.showAndWait();
				 this.clear(e);
				} 
			} 
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate date = LocalDate.parse(view.tfFoundationDate.getText(), formatter);
			
			
			if (name != null && population != 0 && language != null && name.length() > 2) {
				model.saveCanton(name, population, language, date);
				updateView(model.getCanton());
				this.clear(e);
			}
		} catch (NullPointerException | NumberFormatException | DateTimeParseException b) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Ungueltige Eingabe");
			alert.setContentText(b.toString());
			alert.showAndWait();
			this.clear(e);

		}	
	}
	
	private void delete(ActionEvent e) {
		int id = view.IdObjectToUpdate.get();
		Optional<Canton> cantonToDelete = model.cantonsObservableList.stream()
				.filter(canton -> canton.getID().get() == id).findFirst();
		
		model.cantonsObservableList.remove(id);
		view.updateStage.close();
		updateView(model.getCanton());

		
		}
	
	private void update(ActionEvent e) {
		try {
			String name = view.tfUpdateCantonName.getText();
			int population = Integer.parseInt(view.tfUpdatePopulation.getText());
			Language language = view.cmbUpdateLanguage.getValue();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate date = LocalDate.parse(view.tfUpdateFoundationDate.getText(), formatter);
			
			int id = view.IdObjectToUpdate.get();

			if (name != null && population != 0 && language != null && name.length() > 2) {

				model.updateCanton(name, population, language, date, id);
				updateView(model.getCanton());
			}
		} catch (NullPointerException | NumberFormatException | DateTimeParseException b) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Ungueltige Eingabe");
			alert.setContentText(b.toString());
			alert.showAndWait();

		}
		view.updateStage.close();
	}

	private void clear(ActionEvent e) {
		view.tfName.clear();
		view.tfPopulation.clear();
		view.cmbLanguage.setValue(null);
		view.tfFoundationDate.clear();
	}

	private void updateView(Canton canton) {
		if (canton != null) {
			view.lblDataID.setText(canton.getID().toString());
			view.lblDataName.setText(canton.getNameProperty().toString());
			view.lblDataPopulation.setText(canton.getPopulationProperty().toString());
			view.lblDataFoundation.setText(canton.getFoundationDateProperty().toString());

		} else {
			view.lblDataID.setText("");
			view.lblDataName.setText("");
			view.lblDataPopulation.setText("unbekannt");
			view.lblDataFoundation.setText("01/01/1900");
		}

	}
}
