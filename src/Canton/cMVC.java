package Canton;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Version 0 is just the basic program structure,
 * with placeholders in the view
 */
public class cMVC extends Application {
	private cView view;
	private cModel model;
	private cController controller;
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		model = new cModel();
		view = new cView(stage, model);
		controller = new cController(model, view);
		view.start();
	}
}
