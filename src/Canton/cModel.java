package Canton;

import java.time.LocalDate;
import java.util.Optional;
import Canton.Canton.Language;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class cModel {
	private final SimpleObjectProperty<Canton> cantonProperty = new SimpleObjectProperty<>();
	public ObservableList<Canton> cantonsObservableList = FXCollections.observableArrayList();
	
	public void saveCanton(String name, int population, Language language, LocalDate ld) {
		Canton c = new Canton(name, population, language, ld);
		cantonProperty.set(c);
		cantonsObservableList.add(c);
	}
	
	public void updateCanton(String name, int population, Language language, LocalDate foundationDate, int id) {
		
		Optional<Canton> cantonToUpdate = cantonsObservableList.stream()
				.filter(canton -> canton.getID().get() == id).findFirst();
				
		cantonToUpdate.get().setNameOfCanton(name);
		cantonToUpdate.get().setLanguage(language);
		cantonToUpdate.get().setPopulation(population);
		cantonToUpdate.get().setFoundationDate(foundationDate);
		
		
	}
	
	public ObservableList<Canton> addToObservableCantonList(Canton c) { 
		System.out.println("Observable Liste: "+cantonsObservableList);
		 return cantonsObservableList;
	}
	
	public void deleteCanton() {
		cantonProperty.set(null);
	}
	
	public  Canton getCanton() {
		return cantonProperty.get();
	}
	
	public SimpleObjectProperty<Canton> cantonProperty() {
		return cantonProperty;
	}
}
